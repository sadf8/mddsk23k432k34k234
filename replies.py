from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton
from config import MINIMAL_WITHDRAW, MAXIMAL_WITHDRAW, MANAGER_TELE
from config import MINIMAL_DEPOSIT, MAXIMAL_DEPOSIT
from banking import toFixed
from db_modules import cur2UAH
def NO_TRANS(lang):
    return {
        'ru': 'У вас нет транзакций 🤨',
        'ukr': 'В вас немає транзакцій 🤨'
    }[lang]

def ABOUT(lang, name):
    return {
        'ru': 'Привет, '+name+' ✌️\n\n • Этот бот - автоматизированный *валютный мост* между странами *Европы* 😄\n\n • С началом войны многие столкнулись с *проблемами* и *трудностями* межнациональных платежей 😖\n\n • Для решения этой проблемы и существует *наш бот*👍\n\n • Для того чтобы осуществить *перевод* просто пополните счет любым удобным способом, а затем *выведите* средства на нужный адрес ! 😎',
        'ukr': 'Привіт, '+name+' ✌️\n\n • Цей бот - автоматизований *валютний міст* між країнами *Європи* 😄\n\n • З початком війни багато хто зіштовхнувся з *проблемами* та *труднощами* міжнаціональних платежів 😖\n\n • Саме для вирішення цієї проблеми і існує *наш бот*👍\n\n • Для того щоб здійснити *переказ* просто поповніть рахунок будь-яким зручним способом, а потім *виведіть* кошти на потрібну адресу! 😎',
    }[lang]

def START(lang, name):
    return {
        'ru': 'Привет, '+name+' ✌️\n\n • Этот бот - автоматизированный *валютный мост* между странами *Европы* 😄 \n\n • Для того чтобы осуществить *перевод* просто пополните счет любым удобным способом, а затем *выведите* средства на нужный адрес ! 😎\n\nДля того чтобы открыть меню, просто напишите /wallet',
        'ukr': 'Привіт, '+name+' ✌️\n\n • Цей бот - автоматизований *валютний міст* між країнами *Європи* 😄\n\n • Для того щоб здійснити *переказ* просто поповніть рахунок будь-яким зручним способом, а потім *виведіть* кошти на потрібну адресу! 😎\n\nДля того щоб відкрити меню, просто напишіть /wallet',
    }[lang]

def WALLET(val):
    return '''
    💰 *Ваш баланс: '''+str(toFixed(val,2))+'''$*  

    *1$ ≈ ''' +str(cur2UAH('USD'))+'''₴
    1€ ≈ ''' +str(cur2UAH('EURO'))+'''₴*'''

def DEPOSIT(deposit_addr, id, lang):
    return {
        'ru': 'Для того чтобы *пополнить счет*, просто *отправьте желаемую сумму на адрес:* \n\n`'+deposit_addr+'`\n\n В деталях транзакции укажите уникальный id: \n\n`' + str(id)+'`\n\n ❗️ *Внимательно проверьте адрес* и помните о *минимальных и максимальных ограничениях* на перевод!\n\nМы *не сможем* вернуть вам деньги в случае ошибки 🤕\n\nCуммы *меньше '+str(MINIMAL_DEPOSIT)+'$*, а также *больше ' + str(MAXIMAL_DEPOSIT)+'$* не принимаются 🙅‍♀️',
        'ukr': 'Для того щоб *поповнити рахунок*, просто *перекажіть бажану суму на адресу:* \n\n`'+deposit_addr+'`\n\n В деталях транзакції вкажіть унікальний id: \n\n`' + str(id)+'`\n\n ❗️ *Уважно перевірте адресу* та пам\'ятайте про *мінімальні та максимальні обмеження* на переказ!\n\nМи *не зможемо* повернути вам гроші у разі помилки 🤕\n\nCуми *менше '+str(MINIMAL_WITHDRAW)+'$*, а також *більше ' + str(MAXIMAL_WITHDRAW)+'$* не приймаються 🙅‍♀️'
    }[lang]

def CONFIRM_DEPOSIT(lang):
    return {
        'ukr': 'Натисніть сюди, коли зробите переказ ✅',
        'ru': 'Нажмите сюда когда оформите перевод ✅'
    }[lang]

def CHANGE_LANG(lang):
    return {
        'ukr': 'Змінити мову',
        'ru': 'Поменять язык'
    }[lang]

def WITHDRAW(lang):
    return {
        'ukr': 'Зняття коштів',
        'ru': 'Вывод средств'
    }[lang]

def ABOUT_MINI(lang):
    return {
        'ukr': 'Про сервic',
        'ru': 'Про сервис'
    }[lang]

def CHOOSE_REGION(lang):
    return {
        'ukr': 'Оберіть країну банка, з якого ви хочете поповнити рахунок 🏁',
        'ru': 'Выберите страну банка, из которого вы хотите пополнить счет 🏁'
    }[lang]

def CHOOSE_NODE(lang):
    return {
        'ukr': 'Оберіть зручну платiжну систему',
        'ru': 'Выберите удобную платежную систему'
    }[lang]


def DEPOSIT_TY(lang):
    return {
        'ukr': 'Дякуємо, що скористалися нашим сервісом 😊\n\nЯкщо поповнення не обробилось одразу - *не треба панікувати*, середній час обробки: *година-дві* 🙅‍♀️\n\nЯкщо час очікування перевищує добу – напишіть нам *у підтримку* 👨‍💻\n\nДля того щоб повернутись в меню - натиснiть, або напишiть /wallet',
        'ru': 'Спасибо, что воспользовались нашим сервисом 😊\n\nЕсли пополнение не обработалось сразу - *не надо паниковать*, среднее время обработки: *час-два* 🙅‍♀️\n\nЕсли время ожидания превышает сутки - напишите нам *в поддержку* 👨‍💻\n\nДля того чтобы вернуться в меню - нажмите или напишите /wallet',
    }[lang]

def WITHDRAW_CHOOSE_REGION(lang):
    return {
        'ukr': 'Оберіть країну банка одержувача  🏁',
        'ru': 'Выберите страну банка получателя  🏁'
    }[lang]

def MY_TRANS(lang):
    return {
        'ukr': 'Мої транзакції',
        'ru': 'Мои транзакции'
    }[lang]

def HELP(lang):
    return {
        'ukr': 'Підтримка',
        'ru': 'Поддержка'
    }[lang]

def DENIED(lang, pay_id):
    return {
        'ukr': 'Вашу транзацiю №' + pay_id +' було вiдхилено 😔',
        'ru': 'Ваша транзакция №' + pay_id + ' была отклонена 😔',
    }[lang]

def CONFIRMED(lang, pay_id):
    return {
        'ukr': 'Вашу транзацiю `' + pay_id +'` *було пiдтверджено* 🥳\n\nЩоб подивитись оновлений баланс - просто натиснiть /wallet 🧐',
        'ru': 'Ваша транзакция `' + pay_id + '` *была подтверждена* 🥳\n\nЧтобы посмотреть обновленнный баланс - просто нажмите /wallet 🧐',
    }[lang]

def HELP_ADDR(lang):
    return {
        'ukr': 'Якщо ви зіткнулися з помилкою, або маєте питання - напишіть нашому менеджеру: '+MANAGER_TELE + ' 🥰',
        'ru': 'Если вы столкнулись с ошибкой, или есть вопросы - напишите нашему менеджеру: '+MANAGER_TELE + ' 🥰',
    }[lang]

def WITHDRAW_AMOUNT(lang):
    return {
        'ukr': 'Введіть суму виводу в *$* 🤔',
        'ru': 'Введите сумму вывода в *$* 🤔'
    }[lang]

def WITHDRAW_DESTINATION(lang):
    return {
        'ukr': 'Введіть номер картки поповнення 💳',
        'ru': 'Введите номер карточки, на которую будет осуществлен вывод 💳'
    }[lang]


def WITHDRAW_CHECK_DESTINATION(lang, val):
    return {
        'ukr': '`'+val + '`\n\nЦе правильний номер карти? 🤔\n\nЯкщо ви хочете його *змінити*, просто введіть номер *ще раз* ✍️',
        'ru': '`'+val + '`\n\nЭто правильный номер получателя? 🤔\n\nЧтобы *изменить* номер просто введите номер *еще раз* ✍️'
    }[lang]

def WITHDRAW_CHECK_DESTINATION_KB(lang):
    kb_ru = InlineKeyboardMarkup(); 
    kb_ukr = InlineKeyboardMarkup(); 

    BTN_OK_WCDK = InlineKeyboardButton('✅', callback_data='amount_withdraw')
    BTN_CLOSE_WCDK = InlineKeyboardButton('❌ Выйти ❌', callback_data='close_withdraw')
    kb_ru.row(BTN_OK_WCDK, BTN_CLOSE_WCDK)  

    BTN_OK_WCDK = InlineKeyboardButton('✅', callback_data='amount_withdraw')
    BTN_CLOSE_WCDK = InlineKeyboardButton('❌ Вийти ❌', callback_data='close_withdraw')
    kb_ukr.row(BTN_OK_WCDK, BTN_CLOSE_WCDK)  
    return {
        'ukr': kb_ukr,
        'ru': kb_ru
    }[lang]

def TOP_UP(lang):
    return {
        'ukr': 'Поповнення рахунку',
        'ru': 'Пополнить счет'
    }[lang]




def WITHDRAW_CHECK_AMOUNT(lang, val):
    return {
        'ukr': '*'+val + '$*\n\nЦе правильна сума?\n\nЩоб *змінити її* просто *введіть нове значення* ✍️\n\nПам\'ятайте, що суми *менше '+str(MINIMAL_WITHDRAW)+'$*, а також *більше ' + str(MAXIMAL_WITHDRAW)+'$* не приймаються 🙅‍♀️',
        'ru': '*'+val + '$*\n\nЭто правильная сумма?\n\nЧтобы *изменить ее* просто *введите новое значение* ✍️\n\nПомните, что суммы *меньше '+str(MINIMAL_WITHDRAW)+'$*, а также *больше ' + str(MAXIMAL_WITHDRAW)+'$* не принимаются 🙅‍♀️'
    }[lang]

def WITHDRAW_CHECK_AMOUNT_KB(lang):
    kb_ru = InlineKeyboardMarkup(); 
    kb_ukr = InlineKeyboardMarkup(); 

    BTN_OK_WCDK = InlineKeyboardButton('✅', callback_data='finalize_withdraw')
    BTN_CLOSE_WCDK = InlineKeyboardButton('❌ Выйти ❌', callback_data='close_withdraw')
    kb_ru.row(BTN_OK_WCDK, BTN_CLOSE_WCDK)  
    BTN_OK_WCDK = InlineKeyboardButton('✅', callback_data='finalize_withdraw')
    BTN_CLOSE_WCDK = InlineKeyboardButton('❌ Вийти ❌', callback_data='close_withdraw')
    kb_ukr.row(BTN_OK_WCDK, BTN_CLOSE_WCDK)  
    return {
        'ukr': kb_ukr,
        'ru': kb_ru
    }[lang]

def WITHDRAW_CANCEL(lang):
    return {
        'ukr': 'Вивід грошей скасовано ❌',
        'ru': 'Вывод средств отменен ❌',
    }[lang]

def WITHDRAW_RETRY_SHIT(lang):
    return {
        'ukr': 'Введіть корректні дані, будь ласка ❌',
        'ru': 'Пожалуйста, введите корректные данные ❌',
    }[lang]

def WITHDRAW_OK(lang):
    return {
        'ukr': '*Дякуємо*, що скористалися нашим сервісом 😊\n\nЯкщо транзакцiя не обробилось одразу - *не треба панікувати*, середній час обробки: *година-дві* 🙅‍♀️\n\nЯкщо час очікування перевищує добу – напишіть нам *у підтримку* 👨‍💻\n\nДля того щоб повернутись в меню - натиснiть, або напишiть /wallet',
        'ru': '*Спасибо*, что воспользовались нашим сервисом 😊\n\nЕсли вывод не обработалось сразу - *не надо паниковать*, среднее время обработки: *час-два* 🙅‍♀️\n\nЕсли время ожидания превышает сутки - напишите нам *в поддержку* 👨‍💻\n\nДля того чтобы вернуться в меню - нажмите или напишите /wallet',
    }[lang]

from db_modules import get_deposit_by_id
from db_modules import get_withdraw_by_id

def FORMAT_TRANS(id, lang):
    dep = get_deposit_by_id(id)
    wit = get_withdraw_by_id(id)
    res_ukr = []
    res_ru = []
    for i in dep:
        k = ''
        if (i[5] == 0):
            k = '\nСтатус: ' + i[6]
        else:
            k = '\nСумма: ' + str(toFixed(i[5],2)) + '$  ' + '\nСтатус: *`' + i[6]+'`*' 
        res_ru.append('*Ввод ➕\nID: '+i[2]+' '+ k + '*')
        if (i[5] == 0):
            k = '\nСтатус: ' + i[6]
        else:
            k = '\nСума: ' + str(toFixed(i[5],2)) + '$  ' + '\nСтатус: *`' + i[6] +'`*'
        res_ukr.append('*Ввод ➕\nID: '+i[2]+' '+k+'*')
    for i in wit:
        res_ru.append('*Вывод ➖\nID: '+i[2]+'  \nКуда: '+i[4]+'\nСумма: '+str(toFixed(i[6],2))+'$\nСтатус:*`'+i[7]+'`')
        res_ukr.append('*Вивод ➖\nID: '+i[2]+'  \nКуди: '+i[4]+'\nСума: '+str(toFixed(i[6],2))+'$\nСтатус:*`'+i[7]+'`')
    return {
        'ukr': res_ukr,
        'ru': res_ru
    }[lang]
