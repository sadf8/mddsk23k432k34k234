TOKEN         = '5309631326:AAGfQBLfGJNP4ZqmdcrWk5jpnVcQtAiGsEw'      # токен основного бота
MANAGER_TOKEN = '5135506049:AAHDWsG11nEhra2XQyDmpszLHVy9UAz4W3A'      # токен бота для менеджера

MINIMAL_WITHDRAW = 10    # минимальный  вывод ($)
MAXIMAL_WITHDRAW = 1500  # максимальный вывод ($)
MINIMAL_DEPOSIT  = 10    # минимальный  ввод  ($)
MAXIMAL_DEPOSIT  = 5000  # максимальный ввод  ($)

MANAGER_TELE= '@vasiliy\_curalesov'  # телеграм id который будет в поддержке. (перед _ нужно поставить \)
 
MANAGER_IDS = [444938510]   # телеграм id менеджера

PAYMENT_METHODS = [  # способы оплаты
    ['Ukraine', 'UAH', 'Privatbank', 'privatbank_address'],
    ['Ukraine', 'UAH', 'Monobank', 'some_monobank_address'],
    ['Ukraine', 'UAH', 'OTP', 'otp_address'],
    ['Poland', 'PLN', 'Pekao', 'pekao_address'],
    ['Poland', 'PLN', 'PKO', 'PKO_address'],
    ['Germany', 'EURO', 'Deutche bank', 'deutchebank_address'],
    ['Italy', 'EURO', 'Bank de Italia', 'italian_address']
]
