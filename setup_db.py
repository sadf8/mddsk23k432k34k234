import sqlite3
from config import PAYMENT_METHODS

if __name__ == '__main__':
    con = sqlite3.connect('database.db')
    cur = con.cursor()

    initial_id = 1294834338
    create_entries = '''
    CREATE TABLE nodes (
        node_id       INTEGER   PRIMARY KEY,
        country       TEXT      NOT NULL,
        currency      TEXT      NOT NULL,
        name          TEXT      NOT NULL,
        address       TEXT      NOT NULL
    );'''
    cur.execute(create_entries)
    

    for method in PAYMENT_METHODS:
        data = [initial_id, '\''+method[0]+'\'', '\''+method[1]+'\'', '\''+method[2]+'\'', '\''+method[3]+'\'']
        cur.execute('INSERT INTO nodes VALUES ({},{},{},{},{})'.format(*data))
        initial_id += 1
    

    create_cur = '''
    CREATE TABLE currencies (
        name        TEXT        NOT NULL,
        value       FLOAT       NOT NULL
    );'''
    cur.execute(create_cur)

    cur.execute('INSERT INTO currencies VALUES (\'USD\', 36.78)')

    currencies = cur.execute('SELECT DISTINCT currency FROM \'nodes\'').fetchall()
    for c in currencies:
        if (c[0] == 'PLN'):
            cur.execute('INSERT INTO currencies VALUES (\''+c[0]+'\',8.2)')
        elif (c[0] == 'EURO'):
            cur.execute('INSERT INTO currencies VALUES (\''+c[0]+'\',37.43)')
        else:
            cur.execute('INSERT INTO currencies VALUES (\''+c[0]+'\',1.0)')

    create_users = ''' 
    CREATE TABLE users (
        telegram_id     TEXT      NOT NULL UNIQUE,
        balance         DOUBLE,
        language        TEXT      NOT NULl
    );'''

    cur.execute(create_users)


    create_applications = '''
    CREATE TABLE applications_deposit (
	    date            TEXT,
	    telegram_id     INTEGER   NOT NULL,
 		payment_id      TEXT      NOT NULL UNIQUE,
        region          TEXT      NOT NULL,
        amount_native   DOUBLE,
        amount_usd      DOUBLE,
		status          TEXT      NOT NULL,
        node            TEXT      NOT NULL
    );'''
    cur.execute(create_applications)


    create_applications = '''
    CREATE TABLE applications_withdraw (
	    date            TEXT,
	    telegram_id     INTEGER   NOT NULL,
 		payment_id      TEXT      NOT NULL UNIQUE,
        region          TEXT      NOT NULL,
        adress          TEXT      NOT NULL,
        amount_native   DOUBLE,
        amount_usd      DOUBLE   NOT NULL,
		status          TEXT      NOT NULL
    );'''
    cur.execute(create_applications)

    create_deposit_in_progress = '''
    CREATE TABLE deposit_in_progress (
	    date            DATE,
	    telegram_id     INTEGER,
 		payment_id      TEXT,
        type            TEXT,
        deposit_adress  TEXT,
        last_message_id INTEGER,
        region          TEXT,
        chat_id         INTEGER,
        node            TEXT
    );'''
    cur.execute(create_deposit_in_progress)

    create_manager_deposit_in_progress = '''
    CREATE TABLE manager_deposit_in_progress (
        region          TEXT,
        telegram_id     INTEGER,
        pay_id          TEXT,
        amount          DOUBLE,
        active          TEXT
    );'''
    cur.execute(create_manager_deposit_in_progress)

    create_withdraw_in_progress = '''
    CREATE TABLE withdraw_in_progress (
	    date            DATE,
	    telegram_id     INTEGER,
 		payment_id      TEXT,
        type            TEXT,
        withdraw_adress TEXT,
        last_message_id INTEGER,
        amount_usd      TEXT,
        region          TEXT,
        chat_id         INTEGER,
        state           TEXT
    );'''
    cur.execute(create_withdraw_in_progress)
    
    con.commit();
    con.close();


