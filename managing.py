import sqlite3
import logging
from tokenize import String
from config import TOKEN,MANAGER_TOKEN, MANAGER_IDS
from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.helper import Helper, HelperMode, ListItem
from replies import DENIED,CONFIRMED

from db_modules import get_withdraw_by_pay_id,set_deposit_status_by_pay_id,get_deposit_by_pay_id,set_withdraw_status_by_pay_id,add_balance,manager_doing_stuff,get_manager,set_deposit_val,set_manager_money,lang_by_id, nat_curr,cur2UAH, get_regions,set_cur

from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware

bot = Bot(token=MANAGER_TOKEN)
main_bot = Bot(token=TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(LoggingMiddleware())

logging.basicConfig(level=logging.INFO)


async def inform_managers_new_application(id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    if (id[0] != '\''):
        id = '\''+id+'\''
    cur.execute("SELECT * FROM applications_withdraw WHERE payment_id="+id+"")
    wit = cur.fetchall()
    cur.execute("SELECT * FROM applications_deposit WHERE payment_id="+id+"")
    dep = cur.fetchall()

    if len(dep) == 0:
        b1 = InlineKeyboardButton("CONFIRM ✅", callback_data='WCONFIRM_'+wit[0][2])
        b2 = InlineKeyboardButton("DENY ❌", callback_data='WDENY_'+wit[0][2])
        f = float(wit[0][6]) * float(cur2UAH('USD')) / float(cur2UAH(nat_curr(wit[0][3])))
        text = 'NEW APPLICATION\n\nWITHDRAW TO `'+wit[0][3]+'`\nTO CARD: `'+ wit[0][4] +'`\nAMOUNT: `' +str(f)+' '+nat_curr(wit[0][3])+'`\nPAYMENT ID: `' + wit[0][2]+'`\nDATE: `'+wit[0][0]+'`'
    else:
        b1 = InlineKeyboardButton("ADD INFO\n", callback_data='DCONFIRM_'+dep[0][2])
        b2 = InlineKeyboardButton("DENY ❌", callback_data='DDENY_'+dep[0][2])
        text = 'NEW APPLICATION\n\nDEPOSIT\nPAYMENT METHOD: '+dep[0][7]+'\nFROM: `'+dep[0][3]+'`\nPAYMENT ID: `' + dep[0][2]+'`\nDATE: `'+dep[0][0]+'`'
    
    kb = InlineKeyboardMarkup(); 
    kb.row(b1,b2)
    for i in MANAGER_IDS:
        await bot.send_message(i, text, reply_markup=kb,parse_mode="Markdown")


def check_DDconfirm(val):
    if (len(val) < 10):
        return 0
    if val[0:9:] == 'DDCONFIRM':
        return 1
    return 0

def check_Dconfirm(val):
    if (len(val) < 10):
        return 0
    if val[0:9:] == 'DCONFIRM_':
        return 1
    return 0

def check_Ddeny(val):
    if (len(val) < 10):
        return 0
    if val[0:6:] == 'DDENY_':
        return 1
    return 0

def check_Wconfirm(val):
    if (len(val) < 10):
        return 0
    if val[0:9:] == 'WCONFIRM_':
        return 1
    return 0

def check_Wdeny(val):
    if (len(val) < 10):
        return 0
    if val[0:6:] == 'WDENY_':
        return 1
    return 0

@dp.callback_query_handler(lambda callback_query:check_Dconfirm(callback_query.data))
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)

    pay_id = callback_query.data[9:len(callback_query.data):]
    data = get_deposit_by_pay_id(pay_id)[0]
    status = data[6]

    if (status == 'In process'):
        manager_doing_stuff(callback_query.from_user.id, data[3], pay_id,0,'ACTIVE')
        await bot.send_message(callback_query.from_user.id, 'WRITE SUM IN NATIVE CURRENCY ('+nat_curr(data[3])+')')
    else:
        await bot.send_message(callback_query.from_user.id, 'CAN\'T CONFIRM, THE STATUS IS ALREADY `'+status+'`')

def check_set_cur(val):
    regions = get_regions()
    arr = str(val).split()
    if (len(arr) != 2):
        return 0
    if (arr[0] == 'USD'):
        return 1
    for region in regions:
        cur = nat_curr(region[0])
        if (cur == arr[0]):
            return 1
    return 0
        

@dp.message_handler(lambda message: check_set_cur(message['text']))
async def process_message(message: types.Message):
    id = message['from'].id
    manager = get_manager(id)
    if (manager == None):        
        return
    cur = message['text'].split()[0]
    val = message['text'].split()[1]
    set_cur(cur, val)
    await bot.send_message(message['from'].id, 'Settings updated ✅')


@dp.message_handler()
async def check_withdraw_amount(message: types.message):
    id = message['from'].id
    manager = get_manager(id)
    if (manager == None):        
        return
    if (manager[4] == 'ACTIVE'):
        kb = InlineKeyboardMarkup(); 
        b1 = InlineKeyboardButton("CONFIRM ✅", callback_data='DDCONFIRM_'+manager[2])
        kb.row(b1)
        set_manager_money(id, message.text)
        await bot.send_message(id, 'The person depositted `' + message.text + ' ' + nat_curr(manager[0])+'`\n\nIs that right?\n\nIn case of mistake just send me another message with right data',reply_markup=kb,parse_mode='Markdown')

def translate(cur, val):
    return float(val) * float(cur2UAH(cur)) / float(cur2UAH('USD'))

@dp.callback_query_handler(lambda callback_query:check_DDconfirm(callback_query.data))
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    id = callback_query.from_user.id

    pay_id = callback_query.data[10:len(callback_query.data):]
    data = get_deposit_by_pay_id(pay_id)[0]

    manager = get_manager(id)
    if (manager[0] == 'NONE'):
        return

    val_native = manager[3]
    val_usd = translate(nat_curr(manager[0]), val_native)

    status = data[6]

    if (status == 'In process'):
        manager_doing_stuff(id, 'NONE', 'NONE',0,'INACTIVE')
        add_balance(data[1],val_usd)
        set_deposit_val(pay_id, val_usd,val_native,'CONFIRMED')
        await bot.send_message(id, 'STATUS SET TO `CONFIRMED`')#infrom manager
        await main_bot.send_message(data[1], CONFIRMED(lang_by_id(data[1]), data[2]), parse_mode="Markdown") # infrom user
    else:
        await bot.send_message(id, 'CAN\'T CONFIRM, THE STATUS IS ALREADY `'+status+'`')



    

@dp.callback_query_handler(lambda callback_query:check_Ddeny(callback_query.data))
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    pay_id = callback_query.data[6:len(callback_query.data):]
    data= get_deposit_by_pay_id(pay_id)[0]
    status = data[6]
    if (status == 'In process'):
        set_deposit_status_by_pay_id(pay_id, 'DENIED')
        await main_bot.send_message(data[1], DENIED(lang_by_id(data[1]), data[2]), parse_mode="Markdown") # infrom user
        await bot.send_message(callback_query.from_user.id, 'STATUS SET TO `DENIED`')#infrom manager
    else:
        await bot.send_message(callback_query.from_user.id, 'CAN\'T DENY, THE STATUS IS ALREADY `'+status+'`')
    
    #id = telegram_id_by_pay_id(pay_id)
    #bot.send_message(id, WITHDRAW_DENIED(lang(id)),parse_mode="Markdown")




@dp.callback_query_handler(lambda callback_query:check_Wconfirm(callback_query.data))
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    pay_id = callback_query.data[9:len(callback_query.data):]
    data = get_withdraw_by_pay_id(pay_id)[0]
    status = data[7]
    if (status == 'In process'):
        set_withdraw_status_by_pay_id(pay_id, 'CONFIRMED')
        await main_bot.send_message(data[1], CONFIRMED(lang_by_id(data[1]), data[2]), parse_mode="Markdown")#infrom user

        await bot.send_message(callback_query.from_user.id, 'STATUS SET TO `CONFIRMED`')#infrom manager
    else:
        await bot.send_message(callback_query.from_user.id, 'CAN\'T CONFIRM, THE STATUS IS ALREADY `'+status+'`')
    
@dp.callback_query_handler(lambda callback_query:check_Wdeny(callback_query.data))
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    pay_id = callback_query.data[6:len(callback_query.data):]
    data= get_withdraw_by_pay_id(pay_id)[0]
    status = data[7]
    if (status == 'In process'):
        set_withdraw_status_by_pay_id(pay_id, 'DENIED')
        add_balance(data[1],data[6])
        await main_bot.send_message(data[1], DENIED(lang_by_id(data[1]), data[2]), parse_mode="Markdown") # infrom user
        await bot.send_message(callback_query.from_user.id, 'STATUS SET TO `DENIED`') # inform manager
    else:
        await bot.send_message(callback_query.from_user.id, 'CAN\'T DENY, THE STATUS IS ALREADY `'+status+'`')


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
