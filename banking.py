from random import getrandbits, seed, choice
from db_modules import get_balance_by_user_id
from datetime import datetime
import json
import requests
from config import MINIMAL_WITHDRAW, MAXIMAL_WITHDRAW
from string import ascii_letters
  
# defining key/request url

def get_unique_payment_id():
    ID_SIZE = 64
    seed(datetime.now())
    return choice(ascii_letters)+str(getrandbits(ID_SIZE))

def toFixed(val, fx):
    val *= (10.0 ** fx)
    val = round(val)
    val /= (10.0 ** fx)
    return val

def validate_withdraw(telegram_id, val):
    balance = get_balance_by_user_id(telegram_id)[0][0]
    if (str(val).isdecimal() == 0):
        return 0
    if (float(val) < MINIMAL_WITHDRAW):
        return 0
    if (float(val) > MAXIMAL_WITHDRAW):
        return 0
    if (float(val) > float(balance)):
        return 0
    return 1

