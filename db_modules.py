from locale import currency
import sqlite3
from tokenize import String


async def add_application(data):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('INSERT INTO applications_deposit VALUES ({},{},{},{},{},{},{},{})'
    .format(
        data['date'], 
        data['telegram_id'],
        data['payment_id'], 
        '\''+data['region']+'\'',
        data['amount_native'],
        data['amount_usd'],
        '\''+data['status']+'\'',
        '\''+data['node']+'\''
        ))
    cur.execute('DELETE FROM deposit_in_progress WHERE telegram_id='+str(data['telegram_id']))
    con.commit()
    con.close()



async def accept_application_withdraw(data):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('INSERT INTO applications_withdraw VALUES ({},{},{},{},{},{},{},{})'
    .format(
        data['date'], 
        data['telegram_id'],
        data['payment_id'], 
        '\''+data['region']+'\'',
        '\''+data['adress']+'\'',
        data['amount_native'],
        data['amount_usd'],
        '\''+data['status']+'\''
        ))
    cur.execute('DELETE FROM withdraw_in_progress WHERE telegram_id='+str(data['telegram_id']))
    
    balance = float(get_balance_by_user_id(data['telegram_id'])[0][0])
    balance -= float(data['amount_usd'])
    cur.execute('UPDATE users SET balance = \'' + str(balance) + '\' WHERE telegram_id = '+str(data['telegram_id']))
    con.commit()
    con.close()


def get_withdraw_by_pay_id(id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('SELECT * FROM applications_withdraw WHERE payment_id=\''+id+'\'')
    return cur.fetchall()

def set_withdraw_status_by_pay_id(id,val):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('UPDATE applications_withdraw SET status=\''+val+'\' WHERE payment_id=\''+id+'\'')
    con.commit()
    con.close()

def get_deposit_by_pay_id(id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('SELECT * FROM applications_deposit WHERE payment_id = \''+id+'\'')
    return cur.fetchall()

def set_manager_money(id, val):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('UPDATE manager_deposit_in_progress SET amount = \''+str(val)+'\' WHERE telegram_id=\''+str(id)+'\'')
    con.commit()
    con.close()

def set_deposit_val(pay_id, val_usd,val_native,status):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('UPDATE applications_deposit SET amount_native = \''+str(val_native)+'\' WHERE payment_id=\''+pay_id+'\'')
    cur.execute('UPDATE applications_deposit SET amount_usd = \''+str(val_usd)+'\' WHERE payment_id=\''+pay_id+'\'')
    cur.execute('UPDATE applications_deposit SET status = \''+status+'\' WHERE payment_id=\''+pay_id+'\'')
    con.commit()
    con.close()

def add_balance(user_id, val):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    balance = float(get_balance_by_user_id(user_id)[0][0])
    balance += float(val)
    cur.execute('UPDATE users SET balance='+str(balance)+' WHERE telegram_id= \''+str(user_id)+'\'')
    con.commit()
    con.close()
    

def set_deposit_status_by_pay_id(id,val):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('UPDATE applications_deposit SET status=\''+val+'\' WHERE payment_id=\''+id+'\'')
    con.commit()
    con.close()

def get_withdraw_by_id(telegram_id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('SELECT * FROM applications_withdraw WHERE telegram_id='+str(telegram_id))
    return cur.fetchall()

def get_deposit_by_id(telegram_id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('SELECT * FROM applications_deposit WHERE telegram_id='+str(telegram_id))
    return cur.fetchall()

def lang_by_id(telegram_id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('SELECT language FROM \'users\' WHERE telegram_id='+str(telegram_id))
    return cur.fetchall()[0][0]

def set_lang_by_id(telegram_id, lang):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('UPDATE users SET language = \''+lang+'\' WHERE telegram_id='+str(telegram_id))
    con.commit()
    con.close()


def state_by_id(telegram_id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('SELECT state FROM withdraw_in_progress WHERE telegram_id='+str(telegram_id))
    data = cur.fetchall()
    if (len(data) == 0):
        return '0'
    return data[0][0]

def create_new_user(id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    data = [id,'0','\'ukr\'']
    cur.execute('INSERT INTO users VALUES ({},{},{})'.format(*data))
    con.commit()
    con.close()


def get_balance_by_user_id(telegram_id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('SELECT balance FROM \'users\' WHERE telegram_id='+str(telegram_id))
    return cur.fetchall()

def update_current_user_application(telegram_id, field, val):
    con = sqlite3.connect('database.db')
    string_id = str(telegram_id)
    cur = con.cursor()
    if (len(cur.execute('SELECT * FROM deposit_in_progress WHERE telegram_id='+str(telegram_id)).fetchall()) == 0):
        cur.execute('INSERT INTO deposit_in_progress (telegram_id, date) VALUES ('+str(telegram_id)+', \'now\')')
    
    cur.execute('UPDATE deposit_in_progress SET '+field+' = \''+str(val)+'\' WHERE telegram_id = '+str(telegram_id))

    date = cur.execute('SELECT DATE();').fetchall()[0][0]
    cur.execute('UPDATE deposit_in_progress SET date = \'' + date + '\' WHERE telegram_id = '+str(telegram_id))

    con.commit()
    con.close()

def get_user_withdraw(telegram_id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('SELECT * FROM withdraw_in_progress WHERE telegram_id='+str(telegram_id))
    return cur.fetchall()


def update_current_user_withdraw(telegram_id, field, val):
    con = sqlite3.connect('database.db')
    string_id = str(telegram_id)
    cur = con.cursor()
    if (len(cur.execute('SELECT * FROM withdraw_in_progress WHERE telegram_id='+str(telegram_id)).fetchall()) == 0):
        cur.execute('INSERT INTO withdraw_in_progress (telegram_id, date) VALUES ('+str(telegram_id)+', \'now\')')
    
    cur.execute('UPDATE withdraw_in_progress SET '+field+' = \''+str(val)+'\' WHERE telegram_id = '+str(telegram_id))

    date = cur.execute('SELECT DATE();').fetchall()[0][0]
    cur.execute('UPDATE withdraw_in_progress SET date = \'' + date + '\' WHERE telegram_id = '+str(telegram_id))

    con.commit()
    con.close()

def add_node(data):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    assert len(data) == 3
    cur.execute('INSERT INTO nodes VALUES ({},{},{})'.format(*data))
    con.commit()
    con.close()

def get_nodes(country):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    return cur.execute('SELECT * FROM \'nodes\' WHERE country=\''+country+'\'').fetchall()

def get_regions():
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    return cur.execute('SELECT DISTINCT country FROM \'nodes\'')

def set_cur(curr, val):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('UPDATE currencies SET value = \''+str(val)+'\' WHERE name=\''+curr+'\'')
    con.commit()
    con.close()

def cur2UAH(curr):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    return cur.execute('SELECT value FROM \'currencies\' WHERE name=\''+curr+'\'').fetchall()[0][0]

def nat_curr(country):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    return cur.execute('SELECT currency FROM \'nodes\' WHERE country=\''+country+'\'').fetchall()[0][0]


def get_top_up_addr_by_node(node):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    return cur.execute('SELECT address FROM nodes WHERE name=\''+node+'\'').fetchall()[0][0]

def get_region_by_node(node):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    return cur.execute('SELECT country FROM nodes WHERE name=\''+node+'\'').fetchall()[0][0]

def get_last_message_id(telegram_id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    return cur.execute('SELECT last_message_id FROM deposit_in_progress WHERE telegram_id='+str(telegram_id)).fetchall()

def get_chat_id(telegram_id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    return cur.execute('SELECT chat_id FROM deposit_in_progress WHERE telegram_id='+str(telegram_id)).fetchall()

async def accept_deposit_application(telegram_id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()

    data = cur.execute('SELECT * FROM deposit_in_progress WHERE telegram_id='+str(telegram_id)).fetchall()
    data = data[0]
    formatted_data = {}
    formatted_data['date'] = 'DATE(\'now\')'
    formatted_data['telegram_id'] = data[1]
    formatted_data['payment_id'] = '\''+str(data[2])+'\''
    formatted_data['status'] = 'In process'
    formatted_data['region'] = data[6]
    formatted_data['amount_native'] = 0
    formatted_data['amount_usd'] = 0    
    formatted_data['node'] = data[8]


    await add_application(formatted_data)
    return formatted_data['payment_id']

def manager_doing_stuff(id, region, pay_id,money,active):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    data = cur.execute('SELECT * FROM manager_deposit_in_progress WHERE telegram_id='+str(id)).fetchall()
    if (len(data) == 0):
        cur.execute('INSERT INTO manager_deposit_in_progress VALUES (\''+region+'\','+str(id)+',\''+pay_id+'\','+str(money)+',\''+active+'\')')
    else:
        cur.execute('UPDATE manager_deposit_in_progress SET pay_id = \''+pay_id+'\' WHERE telegram_id='+str(id))
        cur.execute('UPDATE manager_deposit_in_progress SET region = \''+region+'\' WHERE telegram_id='+str(id))
        cur.execute('UPDATE manager_deposit_in_progress SET amount = \''+str(money)+'\' WHERE telegram_id='+str(id))
        cur.execute('UPDATE manager_deposit_in_progress SET active = \''+'ACTIVE'+'\' WHERE telegram_id='+str(id))
    con.commit()
    con.close()

def get_manager(id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    data = cur.execute('SELECT * FROM manager_deposit_in_progress WHERE telegram_id='+str(id)).fetchall()
    if (len(data) == 0):
        print('МЕНЕДЖЕРА НЕТ В ТАБЛИЦЕ')
    else:
        return data[0]


def set_manager_is_typing_amout(id):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    cur.execute('UPDATE manager_deposit_in_progress SET language = \''+lang+'\' WHERE telegram_id='+str(telegram_id))
    con.commit()
    con.close()
