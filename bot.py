import logging
from aiogram.utils.helper import Helper, HelperMode, ListItem
from replies import *
from banking import *

import sqlite3

from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton

from managing import dp as manager_dp,inform_managers_new_application

from db_modules import *

from config import TOKEN as API_TOKEN


from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware

bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(LoggingMiddleware())

logging.basicConfig(level=logging.INFO)

@dp.message_handler(state='*',commands=['start'])
async def inactive_info(message: types.Message):
    user_balance = get_balance_by_user_id(message['from'].id)
    if (user_balance == []):
        create_new_user(message['from'].id)
    lang = lang_by_id(message['from'].id)
    await bot.send_message(message['from'].id,START(lang, message['from'].first_name),parse_mode = 'Markdown')

@dp.message_handler(state='*',commands=['wallet'])
async def balance(message: types.Message):
    user_balance = get_balance_by_user_id(message['from'].id)
    if (user_balance == []):
        create_new_user(message['from'].id)
        user_balance = get_balance_by_user_id(message['from'].id)

    lang = lang_by_id(message['from'].id)

    user_balance = user_balance[0][0]

    btn_deposit = InlineKeyboardButton(TOP_UP(lang), callback_data='deposit')
    btn_withdraw = InlineKeyboardButton(WITHDRAW(lang), callback_data='withdraw')
    btn_lang = InlineKeyboardButton(CHANGE_LANG(lang), callback_data='change_lang')
    btn_about = InlineKeyboardButton(ABOUT_MINI(lang), callback_data='about')
    btn_my_trans = InlineKeyboardButton(MY_TRANS(lang), callback_data='my_trans')
    btn_help = InlineKeyboardButton(HELP(lang), callback_data='help')
    kb = InlineKeyboardMarkup(); 
    kb.row(btn_deposit, btn_withdraw)  
    kb.row(btn_lang, btn_about) 
    kb.row(btn_help, btn_my_trans)   

    m = await bot.send_message(message['from'].id, WALLET(user_balance), reply_markup=kb,parse_mode = 'Markdown')

    #update_current_user_application(message['from'].id, 'last_message_id', m.message_id)
    #update_current_user_application(message['from'].id, 'chat_id', message['chat'].id)

@dp.callback_query_handler(lambda callback_query: callback_query.data == 'help')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    lang = lang_by_id(callback_query.from_user.id)
    await bot.answer_callback_query(callback_query.id)
    m = await bot.send_message(callback_query.from_user.id, HELP_ADDR(lang),parse_mode = 'Markdown')


@dp.callback_query_handler(lambda callback_query: callback_query.data == 'my_trans')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    lang = lang_by_id(callback_query.from_user.id)
    await bot.answer_callback_query(callback_query.id)

    text = FORMAT_TRANS(callback_query.from_user.id, lang)
    if (len(text) == 0):
        m = await bot.send_message(callback_query.from_user.id, NO_TRANS(lang),parse_mode = 'Markdown')
    else:
        for i in text:
            m = await bot.send_message(callback_query.from_user.id, i,parse_mode = 'Markdown')

@dp.callback_query_handler(lambda callback_query: callback_query.data == 'change_lang')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    kb = InlineKeyboardMarkup(); 
    btn_ukr = InlineKeyboardButton('🇺🇦', callback_data='lang_ukr')
    btn_ru = InlineKeyboardButton('🇷🇺', callback_data='lang_ru')
    kb.row(btn_ukr, btn_ru)  
    m = await bot.send_message(callback_query.from_user.id, '*- Оберіть мову* \n\n*- Выберите язык*', reply_markup=kb,parse_mode = 'Markdown')

@dp.callback_query_handler(lambda callback_query: callback_query.data == 'lang_ru')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    set_lang_by_id(callback_query.from_user.id, 'ru')
    m = await bot.send_message(callback_query.from_user.id, 'Настройка языка сохранена 👌\n\nЧтобы вернуться в меню просто нажмите /wallet',parse_mode = 'Markdown')


@dp.callback_query_handler(lambda callback_query: callback_query.data == 'lang_ukr')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    set_lang_by_id(callback_query.from_user.id, 'ukr')
    m = await bot.send_message(callback_query.from_user.id, 'Налаштування збережено 👌\n\nЩоб повернутись у головне меню просто натиснiть /wallet',parse_mode = 'Markdown')

@dp.callback_query_handler(lambda callback_query: callback_query.data == 'about')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    lang = lang_by_id(callback_query.from_user.id)
    await bot.answer_callback_query(callback_query.id)
    await bot.send_message(callback_query.from_user.id,ABOUT(lang, callback_query.from_user.first_name),parse_mode = 'Markdown')



@dp.callback_query_handler(lambda callback_query: callback_query.data == 'deposit')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    lang = lang_by_id(callback_query.from_user.id)
    await bot.answer_callback_query(callback_query.id)

    kb = InlineKeyboardMarkup(); 
    regions = get_regions()
    for region in regions:
        btn = InlineKeyboardButton(region[0], callback_data=region[0])
        kb.add(btn)

    m = await bot.send_message(callback_query.from_user.id, CHOOSE_REGION(lang), reply_markup=kb)

    await delete_previous_message(callback_query.from_user.id)
    update_current_user_application(callback_query.from_user.id, 'last_message_id', m.message_id)



@dp.callback_query_handler(lambda callback_query: callback_query.data == 'withdraw')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    lang = lang_by_id(callback_query.from_user.id)
    kb = InlineKeyboardMarkup(); 
    regions = get_regions()
    for region in regions:
        btn = InlineKeyboardButton(region[0], callback_data=region[0]+'_withdraw')
        kb.add(btn)
    m = await bot.send_message(callback_query.from_user.id, WITHDRAW_CHOOSE_REGION(lang), reply_markup=kb)
    await delete_previous_message(callback_query.from_user.id)
    update_current_user_application(callback_query.from_user.id, 'last_message_id', m.message_id)


def check_region_callback_withdraw(val):
    for region in get_regions():
        if region[0] + '_withdraw' == val:
            return 1
    return 0

def check_region_callback(val):
    for region in get_regions():
        if region[0] == val:
            return 1
    return 0

def check_node_callback(val):
    for region in get_regions():
        for node in get_nodes(region[0]):
            if node[3] == val:
                return 1
    return 0


@dp.callback_query_handler(lambda callback_query: check_region_callback_withdraw(callback_query.data))
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    lang = lang_by_id(callback_query.from_user.id)
    region = callback_query.data[0:len(callback_query.data) - len('_withdraw'):]
    update_current_user_withdraw(callback_query.from_user.id, 'region', region)
    update_current_user_withdraw(callback_query.from_user.id, 'state', 'destination')
    m = await bot.send_message(callback_query.from_user.id, WITHDRAW_DESTINATION(lang), parse_mode="Markdown")

    await delete_previous_message(callback_query.from_user.id)
    update_current_user_application(callback_query.from_user.id, 'last_message_id', m.message_id)


@dp.callback_query_handler(lambda callback_query: callback_query.data == 'continue_withdraw')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    lang = lang_by_id(callback_query.from_user.id)
    m = await bot.send_message(callback_query.from_user.id, WITHDRAW_DESTINATION(lang), parse_mode="Markdown")
    #await delete_previous_message(callback_query.from_user.id)
    update_current_user_application(callback_query.from_user.id, 'last_message_id', m.message_id)

@dp.message_handler(lambda message: state_by_id(message['from'].id) == 'destination')
async def process_message(message: types.Message):
    lang = lang_by_id(message['from'].id)
    kb = WITHDRAW_CHECK_DESTINATION_KB(lang)  
    update_current_user_withdraw(message['from'].id, 'withdraw_adress', message.text)
    m = await message.reply(WITHDRAW_CHECK_DESTINATION(lang,message.text), reply=False, reply_markup=kb, parse_mode="Markdown")
    #await delete_previous_message(message['from'].id)
    update_current_user_application(message['from'].id, 'last_message_id', m.message_id)



@dp.callback_query_handler(lambda callback_query: callback_query.data == 'close_withdraw')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    update_current_user_withdraw(callback_query.from_user.id, 'state', '0')
    await bot.answer_callback_query(callback_query.id)
    lang = lang_by_id(callback_query.from_user.id)
    m = await bot.send_message(callback_query.from_user.id, WITHDRAW_CANCEL(lang))


@dp.callback_query_handler(lambda callback_query: callback_query.data == 'amount_withdraw')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    lang = lang_by_id(callback_query.from_user.id)

    update_current_user_withdraw(callback_query.from_user.id, 'state', 'amount')
    m = await bot.send_message(callback_query.from_user.id, WITHDRAW_AMOUNT(lang), parse_mode="Markdown")

    await delete_previous_message(callback_query.from_user.id)
    update_current_user_application(callback_query.from_user.id, 'last_message_id', m.message_id)

@dp.message_handler(lambda message: state_by_id(message['from'].id) == 'amount')
async def process_message(message: types.Message):
    lang = lang_by_id(message['from'].id)
    kb = WITHDRAW_CHECK_AMOUNT_KB(lang)  
    
    if validate_withdraw(message['from'].id, message.text):
        update_current_user_withdraw(message['from'].id, 'amount_usd', message.text)
        m = await message.reply(WITHDRAW_CHECK_AMOUNT(lang,message.text), reply=False, reply_markup=kb, parse_mode="Markdown")
    else:
        m = await message.reply(WITHDRAW_RETRY_SHIT(lang), reply=False, parse_mode="Markdown")
    await delete_previous_message(message['from'].id)
    update_current_user_application(message['from'].id, 'last_message_id', m.message_id)
        

#-------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------
@dp.callback_query_handler(lambda callback_query: callback_query.data == 'finalize_withdraw')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    update_current_user_withdraw(callback_query.from_user.id, 'state', '0')
    await bot.answer_callback_query(callback_query.id)
    lang = lang_by_id(callback_query.from_user.id)
    #if (validate_withdraw(callback_query.from_user.id, ))

    payment_id = get_unique_payment_id()
    
    data = get_user_withdraw(callback_query.from_user.id)
    f_data = {}
    f_data['telegram_id'] = callback_query.from_user.id
    f_data['payment_id'] = '\''+payment_id+'\''
    f_data['amount_usd'] = data[0][6]
    f_data['amount_native'] = '0'
    f_data['date'] = 'DATE(\'now\')'
    f_data['region'] = data[0][7]
    f_data['adress'] = data[0][4]
    f_data['status'] = 'In process'

    await accept_application_withdraw(f_data)

    m = await bot.send_message(callback_query.from_user.id, WITHDRAW_OK(lang), parse_mode="Markdown")
    await delete_previous_message(callback_query.from_user.id)
    update_current_user_application(callback_query.from_user.id, 'last_message_id', m.message_id)
    await inform_managers_new_application(payment_id)

#-------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------

@dp.callback_query_handler(lambda callback_query: check_region_callback(callback_query.data))
async def process_callback_deposit(callback_query: types.CallbackQuery):
    lang = lang_by_id(callback_query.from_user.id)
    await bot.answer_callback_query(callback_query.id)

    kb = InlineKeyboardMarkup(); 
    nodes = get_nodes(callback_query.data)
    for node in nodes:
        btn = InlineKeyboardButton(node[3], callback_data=node[3])
        kb.add(btn)

    m = await bot.send_message(callback_query.from_user.id, CHOOSE_NODE(lang), reply_markup=kb)

    await delete_previous_message(callback_query.from_user.id)
    update_current_user_application(callback_query.from_user.id, 'last_message_id', m.message_id)

@dp.callback_query_handler(lambda callback_query: check_node_callback(callback_query.data))
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    lang = lang_by_id(callback_query.from_user.id)

    payment_id = get_unique_payment_id()
    node = callback_query.data
    region = get_region_by_node(node)
    deposit_addr =  get_top_up_addr_by_node(node)

    update_current_user_application(callback_query.from_user.id, 'payment_id', str(payment_id))
    update_current_user_application(callback_query.from_user.id, 'deposit_adress', str(deposit_addr))
    update_current_user_application(callback_query.from_user.id, 'date', 'DATE()')
    update_current_user_application(callback_query.from_user.id, 'region', region)
    update_current_user_application(callback_query.from_user.id, 'node', node)
    
    kb = InlineKeyboardMarkup(); 
    btn = InlineKeyboardButton(CONFIRM_DEPOSIT(lang), callback_data='confirm_deposit')
    kb.add(btn)
    m = await bot.send_message(callback_query.from_user.id, DEPOSIT(deposit_addr, payment_id, lang), reply_markup=kb,parse_mode="Markdown")

    #await delete_previous_message(callback_query.from_user.id)
    #update_current_user_application(callback_query.from_user.id, 'last_message_id', m.message_id)


@dp.callback_query_handler(lambda callback_query: callback_query.data == 'confirm_deposit')
async def process_callback_deposit(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    lang = lang_by_id(callback_query.from_user.id)

    m = await bot.send_message(callback_query.from_user.id, DEPOSIT_TY(lang), parse_mode="Markdown")
    pay_id = await accept_deposit_application(callback_query.from_user.id)
    await inform_managers_new_application(pay_id)

    #await delete_previous_message(callback_query.from_user.id)
    #update_current_user_application(callback_query.from_user.id, 'last_message_id', m.message_id)


async def delete_previous_message(telegram_id):
    message_id = get_last_message_id(telegram_id)
    chat_id = get_chat_id(telegram_id)
    #if (len(message_id) == 1):
    #    await bot.delete_message(chat_id[0][0], message_id[0][0])




if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
